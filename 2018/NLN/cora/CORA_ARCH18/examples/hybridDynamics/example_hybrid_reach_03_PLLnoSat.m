function completed = example_hybrid_reach_03_PLLnoSat()
% example_hybrid_reach_03_PLLnoSat - example for hybrid dynamics; this 
% example uses continuization to speed up the verification process
%
% This example can be found in
% Althoff, M.; Rajhans, A.; Krogh, B. H.; Yaldiz, S.; Li, X. & Pileggi, L. 
% Formal Verification of Phase-Locked Loops Using Reachability Analysis 
% and Continuization Proc. of the Int. Conference on Computer Aided Design, 
% 2011, 659-666
%
% and
%
% Althoff, M.; Rajhans, A.; Krogh, B. H.; Yaldiz, S.; Li, X. & Pileggi, L. 
% Formal Verification of Phase-Locked Loops Using Reachability Analysis 
% and Continuization Communications of the ACM, 2013, 56, 97-104
%
% Syntax:  example_hybrid_reach_02_powerTrain()
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      20-January-2010
% Last update:  26-May-2018
% Last revision:---


%------------- BEGIN CODE --------------

dim=6;


%states
%x_1 = v_i
%x_2 = v_p1
%x_3 = v_p
%x_4 = phi_v
%x_5 = phi_ref

%set options---------------------------------------------------------------

options.tStart=0; %start time
options.startLoc=2; %initial location 
options.tFinal=inf; %final time
options.finalLoc=0; %0: no final location

options.taylorTerms=10; %number of taylor terms for reachable sets
options.zonotopeOrder=5; %zonotope order
options.polytopeOrder=1.5; %polytope order

% options.filterLength=[dim+4, dim+3]; %filter length for order reduction
% options.tightPolytopeConversion=1; %use tight polytope conversion

options.originContained=0;

options.plotType='filledFrame';

%specify linear system
[options.sys.A, options.sys.B, options.sys.c, options.sys.Ii, options.sys.Ip, N, f0, fref] = initPLL;  


%change A, B, c to capture time as an additional variable
Asim = zeros(6);
Asim(1:4,1:4) = options.sys.A{1};

Bsim = zeros(6,2);
Bsim(1:4,1:2) = options.sys.B{1};

csim = zeros(6,1);
csim(4) = 1/N*f0;
csim(5) = fref;
csim(6) = 1; %time derivative

simSys = linearSys('PLL',Asim,eye(dim)); %initialize system
%--------------------------------------------------------------------------


%create input sets
supFactor = 1.01;
infFactor = 0.99;
vSup=supFactor*[options.sys.Ii; options.sys.Ip];
vInf=infFactor*[options.sys.Ii; options.sys.Ip];
v=interval(vInf,vSup);
for i=1:4
    options.U{i} = options.sys.B{i}*zonotope(v); 
end

%create input for delay
diffFactor = supFactor-infFactor;
vSup_delay=diffFactor*[options.sys.Ii; options.sys.Ip];
vInf_delay=-diffFactor*[options.sys.Ii; options.sys.Ip];
v_delay=interval(vInf_delay,vSup_delay);
for i=1:4
    options.U_delay{i} = options.sys.B{i}*zonotope(v_delay);
end


%locations:
%loc1: off
%loc2: up
%loc3: dw

%projection matrix
P = [1 0 0 0 0 0; 0 1 0 0 0 0; 0 0 1 0 0 0; 0 0 0 1 -1 0];

%define distant
dist = 1e3;
thin = 1e-8;


%loc1:
%input:
options.Uloc{1}=zonotope(csim); 
%invariant
inv=interval([-dist; -dist; -dist; -1-thin; -1-thin; -dist],[dist; dist; dist; 1; 1; dist]);
%inv=intervalhull([-dist, dist; -dist, dist; -dist, dist; -1-thin, 1; -1-thin, 1; -dist, dist]);
%guard set 1 sim
guard1Sim=interval([-dist; -dist; -dist; -dist; 1-thin; -dist],[dist; dist; dist; dist; 1; dist]); 
%guard1Sim=intervalhull([-dist, dist; -dist, dist; -dist, dist; -dist, dist; 1-thin, 1; -dist, dist]); 
%guard set 2 sim
guard2Sim=interval([-dist; -dist; -dist; 1-thin; -dist; -dist],[dist; dist; dist; 1; dist; dist]);
%guard2Sim=intervalhull([-dist, dist; -dist, dist; -dist, dist; 1-thin, 1; -dist, dist; -dist, dist]); 
%reset 1
reset1.A=eye(dim);
reset1.b=[0; 0; 0; -1; -1; 0];
%reset 2
reset2.A=eye(dim);
reset2.b=[0; 0; 0; -1; -1; 0];
%transition 1 sim
transSim{1}=transition(guard1Sim,reset1,2,'a','b'); %--> next loc: 2
%transition 2 sim
transSim{2}=transition(guard2Sim,reset2,3,'a','b'); %--> next loc: 3
%specify location
locSim{1}=location('off',1,inv,transSim,simSys);

%loc2:
%input:
options.Uloc{2}=Bsim*zonotope(v) + csim;
%invariant
inv=interval([-dist; -dist; -dist; -1-thin; -1-thin; -dist],[dist; dist; dist; 0; 1; dist]);
%inv=intervalhull([-dist, dist; -dist, dist; -dist, dist; -1-thin, 0; -1-thin, 1; -dist, dist]);
%guard set 1 sim
guard1Sim=interval([-dist; -dist; -dist; -thin; -dist; -dist],[dist; dist; dist; 0; dist; dist]); 
%guard1Sim=intervalhull([-dist, dist; -dist, dist; -dist, dist; -thin, 0; -dist, dist; -dist, dist]); 
%reset 1
Atmp = eye(dim);
Atmp(6,6) = 0;
reset1.A=Atmp;
reset1.b=zeros(dim,1);
%transition 1 sim
transSim{1}=transition(guard1Sim,reset1,4,'a','b'); %--> next loc: 4
%specify location
locSim{2}=location('up',2,inv,transSim,simSys);


%loc3:
%input:
options.Uloc{3}=-Bsim*zonotope(v) + csim;
%invariant
inv=interval([-dist; -dist; -dist; -1-thin; -1-thin; -dist],[dist; dist; dist; 1; 0; dist]);
%inv=intervalhull([-dist, dist; -dist, dist; -dist, dist; -1-thin, 1; -1-thin, 0; -dist, dist]);
%guard set 1 sim
guard1Sim=interval([-dist; -dist; -dist; -dist; -thin; -dist],[dist; dist; dist; dist; 0; dist]);
%guard1Sim=intervalhull([-dist, dist; -dist, dist; -dist, dist; -dist, dist; -thin, 0; -dist, dist]); 
%reset 1
Atmp = eye(dim);
Atmp(6,6) = 0;
reset1.A=Atmp;
reset1.b=zeros(dim,1);
%transition 1 sim
transSim{1}=transition(guard1Sim,reset1,4,'a','b'); %--> next loc: 4
%specify location
locSim{3}=location('dw',3,inv,transSim,simSys);


%loc4:
%input:
options.Uloc{4}=Bsim*zonotope(v_delay) + csim;
%invariant
inv=interval([-dist; -dist; -dist; -dist; -dist; -dist],[dist; dist; dist; dist; dist; dist]);
%inv=intervalhull([-dist, dist; -dist, dist; -dist, dist; -dist, dist; -dist, dist; -dist, dist]);
%guard set 1 sim
guard1Sim=interval([-dist; -dist; -dist; -dist; -dist; 50e-6],[dist; dist; dist; dist; dist; 51e-6]);
%guard1Sim=intervalhull([-dist, dist; -dist, dist; -dist, dist; -dist, dist; -dist, dist; 50e-6, 51e-6]);
%reset 1
reset1.A=eye(dim);
reset1.b=zeros(dim,1);
%transition 1 sim
transSim{1}=transition(guard1Sim,reset1,1,'a','b'); %--> next loc: 1
%specify location
locSim{4}=location('both',4,inv,transSim,simSys);

% no translating inputs
for i=1:4
    options.uLocTrans{i} = 0;
end


%specify hybrid automaton
HAsim=hybridAutomaton(locSim);


%set enhanced options
%options = initPLLgeneral(-0.2, -0.1, options);
options = initPLLgeneral(-0.5, -0.4, options);
options.viInt = interval(0,0.7);
options.vpInt = interval(-4,12);


% options = initPLLgeneral_noSat_lock(-0.01, 0.01, options);
% options.viInt = infsup(0,0.7);
% options.vpInt = infsup(-0.7,0.7);
%compute reachable set: no saturation
tic
[R,IHstart] = reachPLL_general_noSat(HAsim, options);
tComp_reach=toc;

options.simCycles = length(R);
options.IHstart = IHstart;
%options.simCycles = 10;

%obtain random simulation results
%samples=30;
samples=3;
iSim=1;

tic


while iSim<=samples
    %set initial state, input
    if iSim<=15
        options.x0=[randPointExtreme(options.R0);0;0]; %initial state for simulation
    else
        options.x0=[randPoint(options.R0);0;0]; %initial state for simulation
    end
    
    if options.x0(4)~=options.x0(5)
        
        %set initial location
        if options.x0(4)<options.x0(5)
            options.startLoc=2; %initial location 
        else
            options.startLoc=1; %initial location 
        end
    
        %simulate hybrid automaton
        HAsimRes=simulatePLL(HAsim,options); 

        %get simulation results
        res = get(HAsimRes,'result');
        simRes{iSim} = res.simulation.x_cycle;
        lockCycle{iSim} = res.simulation.lockCycle;
        
        iSim=iSim+1;
    end
    
    iSim
end

tComp_sim=toc;
tComp_sim_avg=tComp_sim/samples;


save R_noSat R simRes lockCycle tComp_reach tComp_sim tComp_sim_avg
load R_noSat


%for parallel execution
R0=options.R0;

Psim = [1 0 0 0 0 0; 0 1 0 0 0 0; 0 0 1 0 0 0; 0 0 0 1 -1 0];
dims=[1 2; 2 3; 1 4; 3 4]; %for plotting only
plotRange = 1:200;

for i=1:4
    
    figure
    hold on
    
    for iSet=plotRange
        
        %plot reachable set
        Rproj = project(R{iSet},dims(i,:));
        Rplot = reduce(Rproj,'girard',20);
        plotFilled(Rplot,[1 2],'w','EdgeColor','b');
    end

%     %plot initial set
%     plot(R0,dims(i,:),'k-');
    
    for iSet=plotRange
        %plot simulation results
        for n=plotRange
            %plot results
            try
                xProj = Psim*simRes{n}(iSet,:)';
                plot(xProj(dims(i,1)),xProj(dims(i,2)),'r.');
                %plot(simRes{n}(iSet,dims(i,1)),simRes{n}(iSet,dims(i,2)),'ro');
            catch
            end
        end
    end
end

%matlabpool close

%example completed
completed = 1;

%------------- END OF CODE --------------
