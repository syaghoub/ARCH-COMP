function r = falstar_validate(in, out, ts, us, ys, phi)
    javaaddpath('../../falstar/scala-library.jar');
    javaaddpath('../../falstar/falstar.jar');

    r = falstar.Main.robustness(in, out, ts, us, ys, phi);
end
